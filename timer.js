/*
** L++ Created by Maury
** @autor MauryDev
** @version 0.1
** @storage timer
** @timer timer_js
*/
// storage timers
function timer() {
    this.timers = {};
    this.deleteTimer = function(name) {
        if (typeof(name) == "string") {
            delete this.timers[name];
        }
    }
    this.scale = 100;
    this.addTimer = function(name) {
        if (typeof(name) == 'string') {
            this.timers[name] = new timer_js(name);
        }
    }
}

// timer javascript
function timer_js(name) {
    this.value_dec = 0;
    this.value = 0;
    this.reset = function() {
        this.value_dec = 0;
    }
    this.name = name;
    this.pause = false;
    this.start = function() {
        function timecheck(value) {
            if (value < 10) {
                return "0." + value;
            } else {
                return Math.floor(value/10) + '.' + (value - (Math.floor(value/10)*10));
            }
        }
        if (!this.pause) {
            if (this.value == "0") {
                console.log("0.0")
            }
            this.value_dec += 1;
            this.value = timecheck(this.value_dec);
            console.log(this.value);
        }
    }
}

// Time Create const
const StorageTime = new timer();

// use code
StorageTime.addTimer("my"); // your timer
StorageTime.scale = 100;
StorageTime.deleteTimer("my")

const my = StorageTime.timers['my'];
my.pause = true; // use false or true
my.reset();
my.pause = false;
console.log(my.value);
console.log(StorageTime.scale);
if (my.paused) {
    console.log('yes')
} else {
    console.log("no")
}

// call inc
setTimeout(call,StorageTime.scale);
function call() {
    // use if
    if (my !== undefined) {
        my.start(); // use code
    }
    setTimeout(call,StorageTime.scale);
    
}